<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 9/10/16
 * Time: 10:09 PM
 */

namespace RCSI\FileSystem;


use RCSI\Exceptions\DirectoryException;

class FileTree
{
    protected $baseDir;

    protected $excludedFiles = array();

    protected $fileTree = array();

    public function getFileTree()
    {
        return $this->fileTree;
    }

    public function setBaseDir($dir)
    {
        $this->baseDir = $dir;
        return $this;
    }

    public function exclude($file)
    {
        $this->excludedFiles[] = $file;
        return $this;
    }

    public function build()
    {
        if ($this->baseDir === null) {
            throw new DirectoryException("Base directory not set for filetree");
        }
        $files = $this->getFiles($this->baseDir);

        foreach ($files as $index => $file) {
            foreach ($this->excludedFiles as $exc) {
                if (preg_match('#^' . $this->baseDir . htmlentities($exc) . '$#', $file) === 1) {
                    unset($files[$index]);
                    continue(2);
                }
            }
            $perms = substr(sprintf('%o', fileperms($file)), -4);
            $group = posix_getgrgid(filegroup($file))['name'];
            $owner = posix_getpwuid(fileowner($file))['name'];
            $isdir = is_dir($file);
            $path  = str_replace($this->baseDir, '', $file);

            $this->fileTree[] = array(
                'permissions' => $perms,
                'owner' => $owner,
                'group' => $group,
                'directory' => $isdir,
                'file' => $path
            );


        }

        return $this;
    }

    protected function getFiles($directory, &$results = array())
    {
        $files = scandir($directory);
        foreach ($files as $key => $value) {
            if ($value === '.' || $value === '..') {
                continue;
            }
            $path = realpath("{$directory}/{$value}");
            $results[] = $path;
            if (is_dir($path)) {
                $this->getFiles($path, $results);
            }
        }
        return $results;
    }

}
