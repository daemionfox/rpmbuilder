<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 9/17/16
 * Time: 4:35 PM
 */

namespace RCSI\Command;


use RCSI\Exceptions\ArgumentException;
use RCSI\Wrapper\GitWrapper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckoutPackage extends AbstractCommand
{

    /**
     * @var GitWrapper
     */
    protected $git;

    /**
     * @return GitWrapper
     */
    public function getGit()
    {
        return $this->git;
    }

    public function configure()
    {
        $this
            ->setName('package:checkout')
            ->setDescription('Clone packages for building')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Package Name'
            )
            ->addArgument(
                'url',
                InputArgument::OPTIONAL,
                'URL of git repository'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pName = $input->getArgument('name');        
        $pURL  = $input->getArgument('url');
        
        $packageName = $pName === null ? self::getPackageName() : $pName;
        $packageURL  = $pURL  === null ? self::getPackageURL()  : $pURL;

        if ($packageName === null || $packageURL === null) {
            throw new ArgumentException("Package name or package url have not been set.");
        }
        
        $progressBar = $this->getProgressBar($output);
        $this->git = new GitWrapper($packageName, $packageURL);
        $this->git->gitClone();
        $progressBar->advance(1);
    }


}
