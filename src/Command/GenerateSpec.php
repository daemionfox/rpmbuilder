<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 9/22/16
 * Time: 4:04 PM
 */

namespace RCSI\Command;

use RCSI\Config\Config;
use RCSI\Connection\Database\Database;
use RCSI\Exceptions\ArgumentException;
use RCSI\Exceptions\GitException;
use RCSI\FileSystem\FileTree;
use RCSI\Wrapper\ConfigWrapper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSpec extends AbstractCommand
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Database
     */
    protected $database;

    public function configure()
    {
        $this
            ->setName('spec:generate')
            ->setDescription('Generates a new spec file for the package')
            ->addArgument(
                'id',
                InputArgument::OPTIONAL,
                'Package Id'
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        if (empty($id)) {
            throw new ArgumentException("Cannot have a null id");
        }

        $this->config = ConfigWrapper::init();
        $this->database = Database::init();
        $generate = array();
        $generate['data'] = $this->getPackageData($id);

        // Now we need to build the git repo:
        /**
         * @var $checkout CheckoutPackage
         */
        $checkout = $this->getApplication()->find('package:checkout');
        $inputForCheckout = new ArrayInput(
            array(
                'command' => 'package:checkout',
                'name' => $generate['data']['name'],
                'url'  => $generate['data']['url']
            )
        );
        $checkoutReturn = $checkout->run($inputForCheckout, $output);
        if ($checkoutReturn !== 0) {
            throw new GitException("Could not clone source");
        }

        $git = $checkout->getGit();
        $tags = $git->gitTag();
        $version = $this->getHighestVersionTag($tags);

        $logs = $this->sanitizeLogs($git->gitLog());

        $generate['git']['version'] = $version;
        $generate['git']['log'] = $logs;
        $generate['options'] = $this->getPackageOptions($id);

        $excludes = array();
        if (isset($generate['options']['excludeFile'])) {
            $excludes = $generate['options']['excludeFile'];
            unset($generate['options']['excludeFile']);
        }
        
        $files = $this->getFiles($git->getBuildDir(), $excludes);
        $generate['fileTree'] = $files;
        print_r($generate);
    }
    
    protected function getFiles($baseDir, $excludes) 
    {
        $tree = new FileTree();
        $tree->setBaseDir($baseDir);
        foreach ($excludes as $e) {
            $tree->exclude($e);
        }
        $tree->build();
        return $tree->getFileTree();
    }

    protected function getPackageOptions($id)
    {
        /** @noinspection SqlResolve */
        $sql = "SELECT optionType AS `type`, optionValue AS `option` FROM buildOptions WHERE packageID = :pid ORDER BY optionType ASC, optionOrdinal ASC, optionID ASC";
        $data = array(":pid" => $id);
        $options = $this->database->queryAll($sql, $data);
        $output = array();
        foreach ($options as $o) {
            $output[$o['type']][] = $o['option'];
        }
        return $output;
    }

    protected function getPackageData($id)
    {

        /** @noinspection SqlResolve */
        $sql = "
          SELECT pkg.packageName AS `name`, pkg.packageSummary AS `summary`, pkg.packageLicense AS `license`, 
          pkg.packageGroup AS `group`, pkg.packageArch AS `architecture`, pkg.packageCompression AS `compression`,
          pkg.packageDescription AS `description`, pkg.packageAutoDependencyCheck AS `autodepcheck`, pkg.packageURL as `url`, 
          CONCAT(own.ownerName, ' <', own.ownerEmail, '>') AS ownerString
          FROM packages AS pkg          
          JOIN owners AS own ON pkg.ownerID = own.ownerID 
          WHERE packageID = :pid
        ";
        $data = array(":pid" => $id);
        $package = $this->database->queryFirst($sql, $data);
        return $package;
    }

    protected function getHighestVersionTag($tags)
    {
        $sortedTags = $tags;
        usort($sortedTags, 'version_compare');
        $tag = array_pop($sortedTags);
        return $tag;
    }

    protected function sanitizeLogs($logs)
    {
        $output = array();
        foreach ($logs as $log) {
            $temp = array(
                'timestamp' => $log['committerDateTimestamp'],
                'owner' => $log['committerName'] . " <" . $log['committerEmail'] . ">",
                'message' => $log['rawBody']
            );
            $temp['date'] = date("D M d Y", $temp['timestamp']);
            $output[] = $temp;
        }
        usort($output, array($this, 'sortLogs'));
        return $output;
    }

    protected function sortLogs($a, $b) {
        if (intval($a['timestamp']) === intval($b['timestamp'])) {
            return 0;
        }
        return (intval($a['timestamp']) < intval($b['timestamp'])) ? 1 : -1;
    }

}
