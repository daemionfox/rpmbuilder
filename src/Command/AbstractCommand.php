<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 9/22/16
 * Time: 4:07 PM
 */

namespace RCSI\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class AbstractCommand extends Command
{


    /**
     * @var ProgressBar
     */
    protected static $progressBar;
    protected static $packageName;
    protected static $packageURL;
    protected static $version;

    /**
     * @param string $version
     */
    public static function setVersion($version)
    {
        self::$version = $version;
    }

    /**
     * @return string
     */
    public static function getVersion()
    {
        return self::$version;
    }

    /**
     * @param ProgressBar $progressBar
     */
    public static function setProgressBar(ProgressBar $progressBar)
    {
        self::$progressBar = $progressBar;
    }

    /**
     * @param OutputInterface $output
     * @return ProgressBar
     */
    public function getProgressBar(OutputInterface $output)
    {
        if (self::$progressBar === null) {
            self::$progressBar = new ProgressBar($output);
            self::$progressBar->start();

        }
        return self::$progressBar;
    }

    /**
     * @return string
     */
    public static function getPackageName()
    {
        return self::$packageName;
    }

    /**
     * @param string $packageName
     */
    public static function setPackageName($packageName)
    {
        self::$packageName = $packageName;
    }

    /**
     * @return string
     */
    public static function getPackageURL()
    {
        return self::$packageURL;
    }

    /**
     * @param string $packageURL
     */
    public static function setPackageURL($packageURL)
    {
        self::$packageURL = $packageURL;
    }



}
