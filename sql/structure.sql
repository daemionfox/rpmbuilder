DROP TABLE IF EXISTS `jobs`;
DROP TABLE IF EXISTS `buildOptions`;
DROP TABLE IF EXISTS `packages`;
DROP TABLE IF EXISTS `owners`;

CREATE TABLE `owners` (
  ownerID INT(11) NOT NULL AUTO_INCREMENT,
  ownerName VARCHAR(255) NOT NULL,
  ownerEmail VARCHAR(255) NOT NULL,
  PRIMARY KEY (ownerID)
);

CREATE TABLE `packages` (
  packageID INT(11) NOT NULL AUTO_INCREMENT,
  packageName VARCHAR(255) NOT NULL,
  packageURL VARCHAR(255) NOT NULL,
  packageKey VARCHAR(255) NOT NULL,
  ownerID INT(11),
  packageLicense VARCHAR(255),
  packageGroup VARCHAR(255),
  packageArch VARCHAR(255),
  packageCompression ENUM('tar', 'tar.gz', 'tar.bz2') NOT NULL DEFAULT 'tar',
  packageSummary TEXT,
  packageDescription TEXT,
  packageAutoDependencyCheck TINYINT(1) NOT NULL DEFAULT '1',

  PRIMARY KEY (packageID),
  FOREIGN KEY (ownerID) REFERENCES owners(ownerID) ON DELETE SET NULL
);

CREATE TABLE `buildOptions` (
  optionID INT(11) NOT NULL AUTO_INCREMENT,
  packageID INT(11) NOT NULL,
  optionType ENUM('require', 'prep', 'build', 'install', 'clean', 'pre', 'post', 'preun', 'postun', 'trigger', 'triggerin', 'triggerun', 'excludeFile', 'setup', 'cleanup'),
  optionValue VARCHAR(255),
  optionOrdinal INT(11),
  PRIMARY KEY (optionID),
  FOREIGN KEY (packageID) REFERENCES packages(packageID) ON DELETE CASCADE
);

CREATE TABLE `jobs` (
  jobID INT(11) NOT NULL AUTO_INCREMENT,
  packageID INT(11),
  packageVersion VARCHAR(255),
  jobProgress FLOAT(3,2) NOT NULL DEFAULT '0',
  jobStatus VARCHAR(255) NULL,
  jobStart TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  jobEnd TIMESTAMP,
  PRIMARY KEY (jobID),
  FOREIGN KEY (packageID) REFERENCES packages(packageID) ON UPDATE CASCADE
)
